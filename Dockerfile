ARG APP_VERSION

FROM apache/superset:$APP_VERSION
# Switching to root to install the required packages
USER root
# Example: installing the MySQL driver to connect to the metadata database
# if you prefer Postgres, you may want to use `psycopg2-binary` instead
RUN pip install mysqlclient psycopg2-binary
# Example: installing a driver to connect to Redshift
# Find which driver you need based on the analytics database
# you want to connect to here:
# https://superset.apache.org/installation.html#database-dependencies
RUN pip install PyAthenaJDBC>1.0.9 PyAthena>1.2.0 \
  sqlalchemy-redshift \
  elasticsearch-dbapi \
  shillelagh[gsheetsapi] \
  pyhive \
  sqlalchemy-trino
# Switching back to using the `superset` user
USER superset
